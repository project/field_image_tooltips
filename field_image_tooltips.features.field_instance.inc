<?php
/**
 * @file
 * field_image_tooltips.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function field_image_tooltips_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'paragraphs_item-image_tooltips-field_tooltip_base_image'.
  $field_instances['paragraphs_item-image_tooltips-field_tooltip_base_image'] = array(
    'bundle' => 'image_tooltips',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_tooltip_base_image',
    'label' => 'Base image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'tooltips/base',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'original',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-image_tooltips-field_tooltip_icon'.
  $field_instances['paragraphs_item-image_tooltips-field_tooltip_icon'] = array(
    'bundle' => 'image_tooltips',
    'deleted' => 0,
    'description' => 'Image that will be used as icon for tooltips. Drupal favicon will be displayed by default.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_tooltip_icon',
    'label' => 'Tooltip icon',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'tooltips/icons',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '30x30',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'original',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-image_tooltips-field_tooltips_content'.
  $field_instances['paragraphs_item-image_tooltips-field_tooltips_content'] = array(
    'bundle' => 'image_tooltips',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Nodes that will be displayed in Ctools popups when user clicks on tooltip icon',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_tooltips_content',
    'label' => 'Tooltips content',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-image_tooltips-field_tooltips_data'.
  $field_instances['paragraphs_item-image_tooltips-field_tooltips_data'] = array(
    'bundle' => 'image_tooltips',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'field_tooltips_data',
        'settings' => array(),
        'type' => 'field_image_tooltip_formatter',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_tooltips_data',
    'label' => 'Tooltips data',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_tooltips_data',
      'settings' => array(),
      'type' => 'field_tooltips_data_widget',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Base image');
  t('Tooltip icon');
  t('Image that will be used as icon for tooltips. Drupal favicon will be displayed by default.');
  t('Tooltips content');
  t('Nodes that will be displayed in Ctools popups when user clicks on tooltip icon');
  t('Tooltips data');

  return $field_instances;
}
